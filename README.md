# _Synmap_

## Чем занимается?

> Добавляет отображение блока с картой на страницу сайта и форму настроек для нее.
> Доступные сервисы карт:

> - Yandex
> - OpenStreetMap
> - Google

## Требования к платформе

- _Drupal 8-11_
- _PHP 7.4.0+_

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/synmap)

```sh
composer require 'drupal/synmap'
```

- [Drupal.org dev версия](https://www.drupal.org/project/synmap/releases/8.x-1.x-dev)

```sh
composer require 'drupal/synmap:1.x-dev@dev'
```

## Как использовать?

- Конфигурационная форма: Конфигурация > Система > SynMap Settings
  Форма предоставляет следующи настройки:

  ### Основные настройки

  - Название компании - Текст отображаемый на метке.
  - Тип карты - Тип карты.
    - [x] Standart Map
    - [ ] Google maps
          (Зависит от языковых настроек и настроек карты)
  - Включить карту (Отображение карты):
    - [ ] На всём сайте
    - [x] На странице контактов
    - [ ] Отключена
  - Путь страницы контактов (Относительный адрес страницы контактов)
  - Прикрепить к (Селектор - место вставки блока с картой)

  ### Дополнтельные настройки карты

  - Широта
  - Долгота
  - Zoom
  - Смещение центра по оси X.
  - Смещение центра по оси Y.
  - Api Key (Ключ API для Yandex карт)
  - Google API Key (Ключ API для Google карт)
  - Insert method (Метод вставки в соответствии с указанным селектором в поле "Прикрепить к")
    - [x] Before (До блока)
    - [ ] After (После блока)
    - [ ] Append (Внутри блока перед закрывающим тегом)
    - [ ] Prepend (Внутри блока после открывающего тега)

  ### Виджет карты

  Позволяет выбрать место для метки на карте с пмощью виджета Yandex карт

  После сохранения конфигурации параметры карты доступны на клиентской части в js переменной DrupalSettings.synmap (массив формируется в hook_page_attachments_alter)

---

# synmap

## Map Integration.

## DEVELOPMENT

1.  HOOK Map display ALTER (see Drupal\synmap\Hook\PageAttachments::hook().

- `&$display` - TRUE/FALSE
- `&$attach` - Map place $.before()

```php
<?
/**
 * Implements hook_synmap_display_alter().
 */
function HOOK_synmap_display_alter(&$display, &$attach) {
  if (!$display && is_object($node = \Drupal::request()->attributes->get('node'))) {
    $display = $node->getType() == 'usluga' ? TRUE : FALSE;
  }
}
```

## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > System > SynMap Settings to
       configure Yandex map settings.
    3. Set configurations and save.

## MAINTAINERS

Supporting organizations:

- Synapse-studio - https://www.drupal.org/synapse-studio
