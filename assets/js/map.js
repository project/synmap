/**
 * @file
 * Author: Synapse-studio.
 */

(function ($) {
  'use strict';

  const lang = drupalSettings.path.currentLanguage;
  const YAPI = 'https://api-maps.yandex.ru/2.1/?lang=' + lang + '_RU';
  const W_H = window.innerHeight;
  const W_W = window.innerWidth;
  var mapData;
  var mapCenterLat;
  var mapCenterLng;
  var init = false;
  var svg = `
    <svg class="synmap-point" width="40" height="40" viewBox="0 0 42 60" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M21 0C7.31177 0 0.54541 9.54 0.54541 21.3082C0.54541 33.0764 21 60 21 60C21 60 41.4545
        33.0764 41.4545 21.3082C41.4545 9.54 34.6881 0 21 0ZM21 28.6364C14.9754 28.6364 10.0909
        23.7518 10.0909 17.7273C10.0909 11.7027 14.9754 6.81818 21 6.81818C27.0245 6.81818 31.909
        11.7027 31.909 17.7273C31.909 23.7518 27.0245 28.6364 21 28.6364Z"/>
    </svg>`;

  $(document).ready(function () {
    prepareMap();
  });

  function prepareMap() {
    var pos = '<div id="synmap"></div>';
    mapData = drupalSettings.synmapReplace ? drupalSettings.synmapReplace : drupalSettings.synmap;
    if (!$(mapData.map.attach).length) {
      return false;
    }
    mapCenterLat = mapData.map.offsetX ? parseFloat(mapData.map.latitude) + parseFloat(mapData.map.offsetX) : mapData.map.latitude;
    mapCenterLng = mapData.map.offsetY ? parseFloat(mapData.map.longitude) + parseFloat(mapData.map.offsetY) : mapData.map.longitude;
    if (W_W < 420) {
      mapCenterLat = mapData.map.latitude;
      mapCenterLng = mapData.map.longitude;
    }
    $(mapData.map.attach)[mapData.map.method](pos);
    loadMap();
  }

  function loadMap() {
    let map = document.getElementById('synmap');

    checkScroll();

    window.addEventListener('scroll', function (e) {
      checkScroll();
    });

    function checkScroll() {
      let condition = window.pageYOffset - (map.offsetTop - 2 * W_H);
      if (condition > 0 && !init) {
        loadScript();
        init = true;
      }
    }
  }

  function loadScript() {
    let script = document.createElement('script');
    let mapType = mapData.map.type;
    if (lang == 'ru') {
      switch (mapType) {
        case 'google':
          window.initMap = function () {
            gMapsInit();
          };
          script.src = 'https://maps.googleapis.com/maps/api/js?key=' + mapData.map.gApikey + '&callback=initMap';
          break
        default: 
          script.onload = function () {
            ymaps.ready(ymapsInit);
          };
          script.src = YAPI;
      }
    }
    else {
      switch (mapType) {
        case 'google':
          window.initMap = function () {
            gMapsInit();
          };
          script.src = 'https://maps.googleapis.com/maps/api/js?key=' + mapData.map.gApikey + '&callback=initMap';
          break
        default: 
          osmInit();
          break
      }
    }
    document.getElementsByTagName('body')[0].appendChild(script);
  }

  function ymapsInit() {
    let map = new ymaps.Map('synmap', {
      center: [mapCenterLat, mapCenterLng],
      zoom: mapData.map.zoom,
      controls: ['zoomControl', 'fullscreenControl']
    });
    map.behaviors.disable('scrollZoom');

    let iconShape = {
      type: 'Rectangle',
      coordinates: [
        [0, 0],
        [41, 60]
      ]
    };
    let iconOffset = [-20, -56];
    let layout = ymaps.templateLayoutFactory.createClass(svg);
    var pointsCollection = new ymaps.GeoObjectCollection();

    $.each(mapData.data, function (index, POINT) {
      let pointLayout = {
        iconLayout: layout,
        iconContent: 'Active',
        iconShape: iconShape,
        iconOffset: iconOffset,
      };

      let Placemark = new ymaps.Placemark([POINT.latitude, POINT.longitude], {
        balloonContentHeader: mapData.data.contact.name,
      }, pointLayout);
      pointsCollection.add(Placemark)
    });
    map.geoObjects.add(pointsCollection);

    // Center Position.
    if (mapData.map.centerAuto === true) {
      var bounds = map.geoObjects.getBounds();
      map.setBounds(bounds, {
        checkZoomRange: true
      }).then(function () {
        if (map.getZoom() > 16) {
          map.setZoom(16);
        }
      });
    }
    else {
      // Move center.
      var position = map.getGlobalPixelCenter();
      map.setGlobalPixelCenter([
        position[0] - mapData.map.centerAutoX,
        position[1] - mapData.map.centerAutoY
      ]);
    }
  }

  function gMapsInit() {
    let LatLng = { lat: parseFloat(mapCenterLat), lng: parseFloat(mapCenterLng) };

    let map = new google.maps.Map(document.getElementById("synmap"), {
      center: LatLng,
      zoom: parseFloat(mapData.map.zoom),
    });

    let marker = new google.maps.Marker({
      map,
      position: LatLng,
      title: mapData.data.contact.name,
    });

    let popup = new google.maps.InfoWindow({
      ariaLabel: mapData.data.contact.name,
      content: "<b>" + mapData.data.contact.name + "</b>",
    });

    marker.addListener("click", () => {
      popup.open({
        anchor: marker,
        map,
      });
    });


  }

  function osmInit() {
    var map = L.map('synmap').setView([mapCenterLat, mapCenterLng], mapData.map.zoom);
    // L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    // L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png', {
    L.tileLayer('https://a.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png', {
      attribution: `
          &copy; <a href="https://carto.com/attribution/" target="_blank">CARTO</a>
        | &copy; <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>`,
      maxZoom: 19,
    }).addTo(map);
    map.attributionControl.setPrefix('');
    var template = L.Util.template(svg);
    var divIcon = L.divIcon({
      className: "leaflet-data-marker",
      html: template,
      iconAnchor: [20, 59],
      iconSize: [41, 60],
      popupAnchor: [0, -56]
    });
    var marker = L.marker([mapCenterLat, mapCenterLng], {
      icon: divIcon
    }).addTo(map);
    marker.on('mouseover', function (e) {
      marker.bindPopup("<b>" + mapData.data.contact.name + "</b>").openPopup();
    });
  }

})(this.jQuery);
