/**
 * @file
 * Author: Synapse-studio.
 */

(function ($) {
  var lang = drupalSettings.path.currentLanguage;
  var YAPI = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
  $(document).ready(function () {

    loadScript();

    function loadScript() {
      var mapSet = drupalSettings.synmap.data;
      if (mapSet.apikey) {
        YAPI = 'https://api-maps.yandex.ru/2.1/?apikey=' + mapSet.apikey + '&lang=ru_RU';
      }
      let script = document.createElement('script');
      script.onload = function() {
        ymaps.ready(ymapsInit);
      };
      script.src = YAPI;
      document.getElementsByTagName('body')[0].appendChild(script);
    }

    function ymapsInit() {
      $('#edit-geo').after('<div id="map-picker" style="min-height:300px"></div>');

      // MAP create.
      var mapSet = drupalSettings.synmap.data;
      var map = new ymaps.Map('map-picker', {
        center: [mapSet.latitude, mapSet.longitude],
        zoom: mapSet.zoom,
        controls: ['zoomControl', 'fullscreenControl']
      });
      var searchControl = new ymaps.control.SearchControl({
        options: {
          provider: 'yandex#search'
        }
      });
      map.controls.add(searchControl);
      map.behaviors.disable('scrollZoom');

      // Point options.
      var defaultParams = {
        geometry: {
          type: 'Point',
          coordinates: [mapSet.latitude, mapSet.longitude]
        },
        properties: {
          iconContent: mapSet.name
        }
      };
      var defaultPreset = {
        preset: 'islands#blackStretchyIcon',
        draggable: true
      };

      // Add Placemark.
      var companyPlacemark = new ymaps.GeoObject(defaultParams, defaultPreset);
      map.geoObjects.add(companyPlacemark);
      // Drag Placemark event.
      companyPlacemark.events.add('dragend', function (e) {
        writeCoords(companyPlacemark);
      });
      // Map click event > change Placemark coords.
      map.events.add('click', function (e) {
        var coords = e.get('coords');
        companyPlacemark.geometry.setCoordinates(coords);
        writeCoords(companyPlacemark);
      });
      // Coords 2 formFields.
      function writeCoords (placemark) {
        var coords = placemark.geometry.getCoordinates();
        $(mapSet.editlatitude).attr('value', coords[0]);
        $(mapSet.editlongitude).attr('value', coords[1]);
      }
    }
  });
})(this.jQuery);
